<?php
/**
 * @file
 * Responsive Web Design Admin
 *
 * @author: Daniel Honrade http://drupal.org/user/351112
 *
 */

/**
 * Admin Form: UI
 *
 */
function rwdimages_admin($form, &$form_state, $op = NULL) {

  global $_rwdimages_set;

  $settings = $_rwdimages_set;

  $form = array();

  $form['rwdimages'] = array(
    '#type'          => 'fieldset',
    '#title'         => 'RWD Images Configuration',
    '#collapsible'   => TRUE,
    '#collapsed'     => FALSE,
    '#tree'          => TRUE,
    '#description'   => t(''),
  );

  $form['rwdimages']['enabled'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Enabled'),
    '#default_value' => $settings['enabled'],
    '#description'   => t('Enable/Disable RWD Images (Useful for testing)'),
    '#prefix'        => '<div class="lazy-columns clearfix"><div class="rwdimages-column rwdimages-column-1">',
  );

  $form['rwdimages']['distance'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Distance'),
    '#default_value' => !empty($settings['distance']) ? $settings['distance']: 0,
    '#size'          => 5,
    '#maxlength'     => 5,
    '#description'   => t('The distance (in pixels) of image to the viewable browser window before it loads the RWD image.'),
  );

  $image_styles = image_styles();
  $options = array('none' => 'None');
  foreach ($image_styles as $option => $val) {
    $options[$option] = $option;
  }

  $form['rwdimages']['breakpoint1'] = array(
    '#type'          => 'select',
    '#title'         => t('Breakpoint Size 1'),
    '#default_value' => $settings['breakpoint1'],
    '#options'       => $options,
    '#description'   => t('Image style for breakpoint 100px'),
  );

  $form['rwdimages']['breakpoint2'] = array(
    '#type'          => 'select',
    '#title'         => t('Breakpoint Size 2'),
    '#default_value' => $settings['breakpoint2'],
    '#options'       => $options,
    '#description'   => t('Image style for breakpoint 200px'),
  );

  $form['rwdimages']['breakpoint3'] = array(
    '#type'          => 'select',
    '#title'         => t('Breakpoint Size 3'),
    '#default_value' => $settings['breakpoint3'],
    '#options'       => $options,
    '#description'   => t('Image style for breakpoint 300px'),
  );

  $form['rwdimages']['breakpoint4'] = array(
    '#type'          => 'select',
    '#title'         => t('Breakpoint Size 4'),
    '#default_value' => $settings['breakpoint4'],
    '#options'       => $options,
    '#description'   => t('Image style for breakpoint 400px'),
  );

  $form['rwdimages']['breakpoint5'] = array(
    '#type'          => 'select',
    '#title'         => t('Breakpoint Size 5'),
    '#default_value' => $settings['breakpoint5'],
    '#options'       => $options,
    '#description'   => t('Image style for breakpoint 500px'),
  );

  $form['rwdimages']['breakpoint6'] = array(
    '#type'          => 'select',
    '#title'         => t('Breakpoint Size 6'),
    '#default_value' => $settings['breakpoint6'],
    '#options'       => $options,
    '#description'   => t('Image style for breakpoint 600px'),
  );

  $form['rwdimages']['breakpoint7'] = array(
    '#type'          => 'select',
    '#title'         => t('Breakpoint Size 7'),
    '#default_value' => $settings['breakpoint7'],
    '#options'       => $options,
    '#description'   => t('Image style for breakpoint 700px'),
  );

  $form['rwdimages']['breakpoint8'] = array(
    '#type'          => 'select',
    '#title'         => t('Breakpoint Size 8'),
    '#default_value' => $settings['breakpoint8'],
    '#options'       => $options,
    '#description'   => t('Image style for breakpoint 800px'),
  );

  $form['rwdimages']['breakpoint9'] = array(
    '#type'          => 'select',
    '#title'         => t('Breakpoint Size 9'),
    '#default_value' => $settings['breakpoint9'],
    '#options'       => $options,
    '#description'   => t('Image style for breakpoint 900px'),
  );

  $form['rwdimages']['breakpoint10'] = array(
    '#type'          => 'select',
    '#title'         => t('Breakpoint Size 10'),
    '#default_value' => $settings['breakpoint10'],
    '#options'       => $options,
    '#description'   => t('Image style for breakpoint for more than 900px'),
    '#suffix'        => '</div>',
  );

  $form['rwdimages']['excludedimages'] = array(
    '#type'          => 'textarea',
    '#title'         => t('Excluded Images'),
    '#default_value' => $settings['excludedimages'],
    '#description'   => t('List images to be excluded from RWD Images separated by comma'),
    '#prefix'        => '<div class="rwdimages-column rwdimages-column-2">',
  );

  $form['rwdimages']['exclude'] = array(
    '#type'          => 'textarea',
    '#title'         => t('Excluded Pages'),
    '#default_value' => $settings['exclude'],
    '#description'   => t('List the page paths to be excluded from RWD Images on each line.'),
    '#suffix'        => '</div></div>',
  );

  $form['save']      = array( '#type' => 'submit', '#value' => t('Save'), );
  $form['reset']     = array( '#type' => 'submit', '#value' => t('Reset'), );

  //dsm($form);
  return $form;
}


/**
 * Admin Form: Submit
 *
 */
function rwdimages_admin_submit($form, $form_state) {

  global $_rwdimages_defaults;

  $form_values = $form_state['values'];

  if ($form_values['op'] == 'Save') {
    $settings = $form_values['rwdimages']; // save user settings
  }
  elseif ($form_values['op'] == 'Reset') {
    $settings = $_rwdimages_defaults;      // save default settings
  }

  variable_set('rwdimages', $settings);
}
