/**
 * @file
 * Responsive Web Design JQuery plugin
 *
 * @author: Daniel Honrade http://drupal.org/user/351112
 *
 * Settings:
 * - distance = distance of the image to the viewable browser screen before it gets loaded
 * - breakpoint1 to breakpoint10 - width in pixels where the right images size loads
 *
 */

(function($){

  // Process rwdimages
  $.fn.rwdimages = function(options){
    var settings = $.extend($.fn.rwdimages.defaults, options);
    var images = this;

    $('body').append('<img id="rwdimage-dummy" src="" alt="" />');
      
    // Load on refresh
    loadRWDImages(images, settings);

    // Load on scroll
    $(window).bind('scroll', function(e){
      if((jQuery().blocklazyloader) || (jQuery().nodelazyloader)) images = $('.block, .node').find('img.rwdimage');
      loadRWDImages(images, settings);
    });

    // Load on resize
    $(window).resize(function(e){
      if((jQuery().blocklazyloader) || (jQuery().nodelazyloader)) images = $('.block, .node').find('img.rwdimage');
      loadRWDImages(images, settings);
    });

    return this;
  };

  // Defaults
  $.fn.rwdimages.defaults = {
    distance: 0,         // the distance (in pixels) of image when loading of the RWD image will happen
    breakpoint1: 'none', // 100px or less
    breakpoint2: 'none', // 200px or less
    breakpoint3: 'none', // 300px or less
    breakpoint4: 'none', // 400px or less
    breakpoint5: 'none', // 500px or less
    breakpoint6: 'none', // 600px or less
    breakpoint7: 'none', // 700px or less
    breakpoint8: 'none', // 800px or less
    breakpoint9: 'none', // 900px or less
    breakpoint10: 'none' // 901px or more      
  };


  // Loading RWD images
  function loadRWDImages(images, settings){
    images.each(function(){
      $(this).parent().css({ display: 'block' });
      var imageContainerHeight = $(this).parent().height();
      var imageContainerWidth = $(this).parent().width();


      if (windowView(this, settings) && $(this).hasClass('rwdimage')){
        loadImage(this, settings, imageContainerHeight, imageContainerWidth);
      }
    });
  };


  // Check if the images are within the window view (top, bottom, left and right)
  function windowView(image, settings){

        // window variables
    var windowHeight = $(window).height(),
        windowWidth  = $(window).width(),

        windowBottom = windowHeight + $(window).scrollTop(),
        windowTop    = windowBottom - windowHeight,
        windowRight  = windowWidth + $(window).scrollLeft(),
        windowLeft   = windowRight - windowWidth,

        // image variables
        imageHeight  = $(image).height(),
        imageWidth   = $(image).width(),

        imageTop     = $(image).offset().top - settings['distance'],
        imageBottom  = imageTop + imageHeight + settings['distance'],
        imageLeft    = $(image).offset().left - settings['distance'],
        imageRight   = imageLeft + imageWidth + settings['distance'];

           // This will return true if any corner of the image is within the screen 
    return (((windowBottom >= imageTop) && (windowTop <= imageTop)) || ((windowBottom >= imageBottom) && (windowTop <= imageBottom))) &&
           (((windowRight >= imageLeft) && (windowLeft <= imageLeft)) || ((windowRight >= imageRight) && (windowLeft <= imageRight)));
  };


  // Load the image
  function loadImage(image, settings, imageContainerHeight, imageContainerWidth){
    var src = $(image).attr('src'),
        breakpoint1  = src.search(settings['breakpoint1']),
        breakpoint2  = src.search(settings['breakpoint2']),
        breakpoint3  = src.search(settings['breakpoint3']),
        breakpoint4  = src.search(settings['breakpoint4']),
        breakpoint5  = src.search(settings['breakpoint5']),
        breakpoint6  = src.search(settings['breakpoint6']),
        breakpoint7  = src.search(settings['breakpoint7']),
        breakpoint8  = src.search(settings['breakpoint8']),
        breakpoint9  = src.search(settings['breakpoint9']),
        breakpoint10 = src.search(settings['breakpoint10']);
    var imageHeight, imageWidth, currentBreakpoint; 
    
    // check the real image size by loading it again on dummy img  
    $('#rwdimage-dummy').attr('src', src).load(function() {
      imageHeight = this.height;
      imageWidth = this.width;
      
      // this will only trigger if the image is less than it's container
      if ((imageContainerHeight > imageHeight) && (imageContainerWidth > imageWidth)) {
        
        // get the current breakpoint
        if(breakpoint1 != -1) {
          currentBreakpoint = settings['breakpoint1'];
        }
        else if(breakpoint2 != -1) {
          currentBreakpoint = settings['breakpoint2'];
        }    
        else if(breakpoint3 != -1) {
          currentBreakpoint = settings['breakpoint3'];
        }
        else if(breakpoint4 != -1) {
          currentBreakpoint = settings['breakpoint4'];
        }
        else if(breakpoint5 != -1) {
          currentBreakpoint = settings['breakpoint5'];
        }
        else if(breakpoint6 != -1) {
          currentBreakpoint = settings['breakpoint6'];
        }
        else if(breakpoint6 != -1) {
          currentBreakpoint = settings['breakpoint7'];
        }
        else if(breakpoint7 != -1) {
          currentBreakpoint = settings['breakpoint8'];
        }
        else if(breakpoint9 != -1) {
          currentBreakpoint = settings['breakpoint9'];
        }
        else if(breakpoint10 != -1) {
          currentBreakpoint = settings['breakpoint10'];
        }
        else {
          currentBreakpoint = '';
        }        
        currentBreakpoint = '/' + currentBreakpoint + '/';
        
        // replace the current image with appropriate size
        if((settings['breakpoint2'] != 'none') && (imageContainerWidth <= 200)) {
          $(image).attr('src', src.replace(currentBreakpoint, '/' + settings['breakpoint2'] + '/'));
        }
        else if((settings['breakpoint3'] != 'none') && (imageContainerWidth <= 300)) {
          $(image).attr('src', src.replace(currentBreakpoint, '/' + settings['breakpoint3'] + '/'));
        }    
        else if((settings['breakpoint4'] != 'none') && (imageContainerWidth <= 400)) {
          $(image).attr('src', src.replace(currentBreakpoint, '/' + settings['breakpoint4'] + '/'));
        }
        else if((settings['breakpoint5'] != 'none') && (imageContainerWidth <= 500)) {
          $(image).attr('src', src.replace(currentBreakpoint, '/' + settings['breakpoint5'] + '/'));
        }
        else if((settings['breakpoint6'] != 'none') && (imageContainerWidth <= 600)) {
          $(image).attr('src', src.replace(currentBreakpoint, '/' + settings['breakpoint6'] + '/'));
        }
        else if((settings['breakpoint7'] != 'none') && (imageContainerWidth <= 700)) {
          $(image).attr('src', src.replace(currentBreakpoint, '/' + settings['breakpoint7'] + '/'));
        }
        else if((settings['breakpoint8'] != 'none') && (imageContainerWidth <= 800)) {
          $(image).attr('src', src.replace(currentBreakpoint, '/' + settings['breakpoint8'] + '/'));
        }
        else if((settings['breakpoint9'] != 'none') && (imageContainerWidth <= 900)) {
          $(image).attr('src', src.replace(currentBreakpoint, '/' + settings['breakpoint9'] + '/'));
        }
        else if((settings['breakpoint10'] != 'none') && (imageContainerWidth > 900)) {
          $(image).attr('src', src.replace(currentBreakpoint, '/' + settings['breakpoint10'] + '/'));
        }
        else {
          $(image).attr('src', src.replace(currentBreakpoint, '/' + settings['breakpoint2'] + '/'));
        }
      }
    });
  };

})(jQuery); 
